from flask import Flask, request, jsonify
from lib.main import SimpleThreeDOF
import logging


app = Flask(__name__)
# logging.basicConfig(filename="server.log", level=logging.DEBUG)


@app.route("/simple-3dof", methods=["POST"])
def hello():
    req = request.get_json()
    sim = SimpleThreeDOF(
        float(req["mass"]),
        float(req["diameter"]),
        float(req["drag_coeff"]),
        float(req["motor_mass"]),
        float(req["motor_tot_imp"]),
        req["thrust_curve_file"],
    )
    results = sim.run()
    # print(results)
    # return "Gefp"
    accels = results[:, 4]
    velocs = results[:, 3]
    alts = results[:, 2]
    final_mass = results[-1, 1]
    time = results[-1, 0]
    return jsonify(
        {
            "apogee": max(alts),
            "max_velocity": max(velocs),
            "max_acceleration": max(accels),
            "time_of_flight": time,
            "final_mass": final_mass,
        }
    )


if __name__ == "__main__":
    app.run(host="localhost", port=6001)

