# Atmosphere is a convenience class that generates pressure, temperature, and
# density values depending on altitude. It uses models pulled from NASA:
#   Imperial model: https://www.grc.nasa.gov/www/k-12/airplane/atmos.html
#   Metric model:   https://www.grc.nasa.gov/www/k-12/airplane/atmosmet.html
#
# They're not spot on but close enough for now.

from enum import Enum
from math import exp, sqrt

# Some conversion constants
TOKELVIN = 273.1
TORANKINE = 459.7


class System(Enum):
    Metric = 1
    Imperial = 2


class AtmosLayer(Enum):
    Troposphere = 1
    LowerStratos = 2
    UpperStratos = 3


class Atmosphere:
    """
    Atmosphere is an atmospheric data engine, which should make it a bit
    easier to perform calculations at different altitudes. It acts like a
    generator, since we're probably going to use metric units at some point. 
    There's also different layers in the atmosphere, so what this class does is 
    determine what models to use given the unit system when an instance of this 
    class is constructed, and use the altitude to determine which model to use 
    for depending on the atmosphere layer. That way, we only have to call 
    Atmosphere.pressure(alt) and let the class do the rest.
    """

    def __init__(self, unitSystem: System):
        """
        Initialize an atmosphere instance with a unit system,
        so the class knows what units we're working with.
        """
        self.unitSystem = unitSystem

    def pressure(self, altitude):
        if self.unitSystem == System.Metric:
            return _metric_pressure(altitude)
        elif self.unitSystem == System.Imperial:
            return _imperial_pressure(altitude)

    def temperature(self, altitude):
        if self.unitSystem == System.Metric:
            return _metric_temperature(altitude)
        elif self.unitSystem == System.Imperial:
            return _imperial_temperature(altitude)

    def density(self, altitude):
        if self.unitSystem == System.Metric:
            return _metric_density(altitude)
        elif self.unitSystem == System.Imperial:
            return _imperial_density(altitude)

    def speed_of_sound(self, altitude):
        """ 
        Probably not the best to hardcode in the values inside
        the functions, but we're only planning on flying in Earth's
        atmosphere, so it's probably fine for now.
        """
        if self.unitSystem == System.Metric:
            return _metric_speed_of_sound(altitude)
        elif self.unitSystem == System.Imperial:
            return _imperial_speed_of_sound(altitude)


# The functions below are what the Atmosphere class uses to generate values,
# but they're not really intended to be used outside of the file, hence the _
# before every function name.


def _metric_model(altitude):
    if altitude < 11000:
        return AtmosLayer.Troposphere
    elif 11000 <= altitude < 25000:
        return AtmosLayer.LowerStratos
    else:
        return AtmosLayer.UpperStratos


def _metric_pressure(altitude):
    model = _metric_model(altitude)
    temp = _metric_temperature(altitude)
    if model == AtmosLayer.Troposphere:
        return 101.29 * ((temp + TOKELVIN) / 288.08) ** 5.256
    elif model == AtmosLayer.LowerStratos:
        return 22.65 * exp(1.73 - 0.000157 * altitude)
    elif model == AtmosLayer.UpperStratos:  # ff0000
        return 2.488 * ((temp + TOKELVIN) / 216.6) ** -11.388  # ff0000


def _metric_temperature(altitude):
    model = _metric_model(altitude)
    if model == AtmosLayer.Troposphere:
        return 15.04 - 0.00649 * altitude
    elif model == AtmosLayer.LowerStratos:
        return -56.46
    elif model == AtmosLayer.UpperStratos:
        return -131.21 + 0.00299 * altitude


def _metric_density(altitude):
    pressure = _metric_pressure(altitude)
    temp = _metric_temperature(altitude)
    return pressure / (0.2869 * (temp + TOKELVIN))


def _metric_speed_of_sound(altitude):
    temp = _metric_temperature(altitude) + TOKELVIN
    return sqrt(1.4 * 287.05 * temp)


def _imperial_model(altitude):
    if altitude < 36152:
        return AtmosLayer.Troposphere
    elif 36152 <= altitude < 82345:
        return AtmosLayer.LowerStratos
    else:
        return AtmosLayer.UpperStratos


def _imperial_pressure(altitude):
    model = _imperial_model(altitude)
    temp = _imperial_temperature(altitude)
    if model == AtmosLayer.Troposphere:
        return 2116 * ((temp + TORANKINE) / 518.6) ** 5.256
    elif model == AtmosLayer.LowerStratos:
        return 473.1 * exp(1.73 - 0.000048 * altitude)
    elif model == AtmosLayer.UpperStratos:
        return 51.97 * ((temp + TORANKINE) / 389.98) ** -11.388


def _imperial_temperature(altitude):
    model = _imperial_model(altitude)
    if model == AtmosLayer.Troposphere:
        return 59 - 0.00356 * altitude
    elif model == AtmosLayer.LowerStratos:
        return -70
    elif model == AtmosLayer.UpperStratos:
        return -205.05 + 0.00164 * altitude


def _imperial_density(altitude):
    pressure = _imperial_pressure(altitude)
    temp = _imperial_temperature(altitude)
    return pressure / (1718 * (temp + TORANKINE))


def _imperial_speed_of_sound(altitude):
    temp = _imperial_temperature(altitude) + TORANKINE
    return sqrt(1.4 * 1718 * temp)


if __name__ == "__main__":
    atmos = Atmosphere(System.Imperial)
    altitude = 35000
    print(f"Altitude:    {altitude:>10} ft")
    print(f"Density:     {atmos.density(altitude):>10.3e} slug/ft3")
    print(f"Pressure:    {atmos.pressure(altitude):>10.3f} psf")
    print(f"Temperature: {atmos.temperature(altitude) + TORANKINE:>10.3f} R")
    print(f"Sonic Speed: {atmos.speed_of_sound(altitude):>10.3f} ft/s")
    print(f"Layer: {_imperial_model(altitude)}")
