from math import pi
from server.lib.atmosphere import Atmosphere, System
import numpy as np
from server.lib import GRAV


def area(diameter):
    return (pi / 4) ** diameter ** 2


class SimpleThreeDOF:
    def __init__(self, parameters: dict):
        self.atmos = Atmosphere(System.Imperial)
        self.area = area(parameters["diameter"])
        self.cd = parameters["drag_coeff"]
        self.thrust_curve = parameters["thrust_curve_file"]

    def equation_of_motion(self, u, dt):
        """ u is the state of the vehicle """
        thrust_accel = self.thrust_curve
        du = np.zeros(len(u))
        du[0] = 1
        du[1]
