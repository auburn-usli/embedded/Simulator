import numpy as np
import random
import matplotlib.pyplot as plt
import pandas as pd
from lib.util import *
from math import log

NEWTTOLB = 0.224809
DT = 0.05

L2200G = pd.read_csv("./lib/L2200G", delim_whitespace=True, header=None)


def print_fixed_width(msg, data):
    print(f"{msg:<15}{data:>10.3f}")


def mass_flow_rate(thrust, total_imp, prop_weight):
    spec_imp = total_imp / prop_weight  # total impulse divided by propellant weight
    return -thrust / (spec_imp * 32.174)


def area(diameter):
    return pi * ((diameter / 12) ** 2) / 4


class SimpleThreeDOF:
    """A simplified 3-DOF simulation that assumes STP conditions. """

    def __init__(
        self, mass, diameter, drag_coeff, motor_mass, motor_tot_imp, thrust_curve_file
    ):
        self.mass = mass
        self.diameter = diameter
        self.drag_coeff = drag_coeff
        self.motor_mass = motor_mass
        self.motor_tot_imp = motor_tot_imp
        self.thrust_curve_file = thrust_curve_file

    def EOM(self, u, p, dt):
        """
        du[0] = change in mass
        du[1] = velocity
        du[2] = acceleration
        """
        du = np.zeros(len(u))
        tot_imp = self.motor_tot_imp
        prop_weight = self.motor_mass

        A = area(self.diameter)
        g = 32.174
        Cd = self.drag_coeff
        thrust = np.interp(u[0], L2200G[0], L2200G[1]) * NEWTTOLB

        du[0] = 1
        du[1] = (
            mass_flow_rate(thrust, tot_imp, prop_weight) * g
        )  # multiply by g to get lbm
        du[2] = u[3]  # velocity
        du[3] = (
            -g
            - (Cd * density(u[2]) * A * u[3] ** 2) / (2 * u[1])
            + (thrust / (u[1] / g))
        )  # acceleration
        u[4] = du[3]
        return du

    def run(self):
        u = np.array([0, self.mass, 0, 0, 0])
        p = np.zeros(2)
        u_vecs = u
        while u[3] > -1:
            u, du = runge_kutta(self.EOM, u, p, 0.1)
            u_vecs = np.vstack((u_vecs, u))
        return u_vecs

